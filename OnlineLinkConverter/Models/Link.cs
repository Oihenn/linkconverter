﻿using OnlineLinkConverter.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineLinkConverter.Models
{
    public class Link
    {
        public int Id { get; set; }
        public string BaseLink { get; set; }
        public string ConvertedLink { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public int GroupId { get; set; }
        public Group Group { get; set; }

        
    }
}
