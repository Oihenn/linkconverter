﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using OnlineLinkConverter.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineLinkConverter.Data
{
    public class DbObjects
    {
        ApplicationDbContext applicationDbContext;
        public static void Initial(ApplicationDbContext applicationDbContext)
        { 
            if (!applicationDbContext.Groups.Any())
            {
                applicationDbContext.Groups.Add(new Group { Name = "Социальные сети", Description = "Интересные профили и группы разных социальных сетей" });
                applicationDbContext.Groups.Add(new Group { Name = "Игровые площадки", Description = "Различные игровые площадки и игры предоставляемые ими" });

                applicationDbContext.SaveChanges();

                applicationDbContext.Links.Add(new Link
                {
                    BaseLink = "https://vk.com/qw1prum?w=wall-30022666_320846",
                    ConvertedLink = "https://olc.user.qwer123/",
                    Description = "Интересная статья",
                    GroupId = applicationDbContext.Groups.First().Id
                });
                applicationDbContext.Links.Add(new Link
                {
                    BaseLink = "https://vk.com/qw1prum?w=wall-30234466_309823",
                    ConvertedLink = "https://olc.user.qwer124/",
                    Description = "Смешная картинка",
                    GroupId = applicationDbContext.Groups.First().Id
                });                    
                applicationDbContext.Links.Add(new Link
                {
                    BaseLink = "https://vk.com/qw1prum?w=wall-23443466_234446",
                    ConvertedLink = "https://olc.user.qwer125/",
                    Description = "Смешная статья",
                    GroupId = applicationDbContext.Groups.First().Id
                });
                applicationDbContext.Links.Add(new Link
                {
                    BaseLink = "https://vk.com/qw1prum?w=wall-38502745_057386",
                    ConvertedLink = "https://olc.user.qwer126/",
                    Description = "Интересная картинка",
                    GroupId = applicationDbContext.Groups.First().Id
                });
                applicationDbContext.Links.Add(new Link
                {
                    BaseLink = "https://vk.com/qw1prum?w=wall-95738648_295738",
                    ConvertedLink = "https://olc.user.qwer127/",
                    Description = "Смешная картинка",
                    GroupId = applicationDbContext.Groups.First().Id
                });
                applicationDbContext.Links.Add(new Link
                {
                    BaseLink = "https://steam.com/qw1prum?w=wall-9537548_295738",
                    ConvertedLink = "https://olc.user.qwer235/",
                    Description = "Ролевая игра",
                    GroupId = applicationDbContext.Groups.Last().Id
                });
                applicationDbContext.Links.Add(new Link
                {
                    BaseLink = "https://steam.com/qw1prum?w=wall-25793838_291234",
                    ConvertedLink = "https://olc.user.qwer236/",
                    Description = "Стрелялка",
                    GroupId = applicationDbContext.Groups.Last().Id
                });
                applicationDbContext.Links.Add(new Link
                {
                    BaseLink = "https://steam.com/qw1prum?w=wall-53492648_423538",
                    ConvertedLink = "https://olc.user.qwer237/",
                    Description = "Моба",
                    GroupId = applicationDbContext.Groups.Last().Id
                });
                applicationDbContext.Links.Add(new Link
                {
                    BaseLink = "https://steam.com/qw1prum?w=wall-12334548_093874",
                    ConvertedLink = "https://olc.user.qwer238/",
                    Description = "Батл роял",
                    GroupId = applicationDbContext.Groups.Last().Id
                });

                applicationDbContext.SaveChanges();                
            }
        }    
    }
}

