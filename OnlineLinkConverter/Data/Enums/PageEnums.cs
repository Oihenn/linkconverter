﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineLinkConverter.Data.Enums
{
    public class PageEnums
    {
        public enum PageType
        {
            Group = 0,
            Link = 1
        }
    }
}
