﻿using OnlineLinkConverter.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineLinkConverter.Data.ViewModels
{
    public class GroupViewModel : PageViewModel
    {
        public Group Group { get; set; }
        public List<LinkViewModel> Links { get; set; }
    }

    public class GroupEditModel : PageEditModel
    {
        public string Name { get; set; }
        public List<Link> Links { get; set; }
        public string UserId { get; set; }
    }
}
