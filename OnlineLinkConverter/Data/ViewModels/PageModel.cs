﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineLinkConverter.Data.ViewModels
{
    public abstract class PageViewModel { }

    public abstract class PageEditModel 
    {
        public int Id { get; set; }
        public string Description { get; set; }
    }
}
