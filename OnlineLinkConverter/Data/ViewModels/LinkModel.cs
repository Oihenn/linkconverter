﻿using OnlineLinkConverter.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineLinkConverter.Data.ViewModels
{
    public class LinkViewModel : PageViewModel
    {
        public Link Link { get; set; }
    }

    public class LinkEditModel : PageEditModel
    {
        public string BaseLink { get; set; }
        public string ConvertedLink { get; set; }
        public bool IsActive { get; set; }
        public int GroupId { get; set; }
        public Group Group { get; set; }
    }
}
