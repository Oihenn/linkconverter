﻿using Microsoft.AspNetCore.Mvc;
using OnlineLinkConverter.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineLinkConverter.Data.Interfaces
{
    public interface ILink
    {
        IEnumerable<Link> GetAllLinks { get; }
        Link GetLinkById(int linkId, bool includeGroup = false);
        void AddLink(Link link);
        void DeleteLinkById(int linkId);
        void ConvertLink(Link link);
    }
}
