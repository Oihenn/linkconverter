﻿using OnlineLinkConverter.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineLinkConverter.Data.Interfaces
{
    public interface IGroup
    {

        //IEnumerable<Group> GetAllGroups { get; }
        IEnumerable<Group> GetAllGroups(string userId); 
        Group GetGroupById(int groupId, bool includeLinks = false);        
        void AddGroup(Group group);
        void DeleteGroupById(int groupId);
    }
}
