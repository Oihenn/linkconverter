﻿using OnlineLinkConverter.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineLinkConverter.Data.Repository
{
    public class RepositoryManager
    {
        public readonly ILink Links;
        public readonly IGroup Groups;

        public RepositoryManager(ILink linkRepository, IGroup groupRepository)
        {
            this.Links = linkRepository;
            this.Groups = groupRepository;
        }

        //public LinkRepository Links { get { return linkRepository; } }
        //public GroupRepository Groups { get { return groupRepository; } }

    }
}
