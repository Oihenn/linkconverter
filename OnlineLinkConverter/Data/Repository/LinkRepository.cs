﻿using Microsoft.EntityFrameworkCore;
using OnlineLinkConverter.Data.Interfaces;
using OnlineLinkConverter.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineLinkConverter.Data.Repository
{
    public class LinkRepository : ILink
    {
        readonly ApplicationDbContext applicationDbContext;

        public LinkRepository(ApplicationDbContext applicationDbContext)
        {
            this.applicationDbContext = applicationDbContext;
        }

        public IEnumerable<Link> GetAllLinks => applicationDbContext.Links;

        public Link GetLinkById(int linkId, bool includeGroup = false)
        {
            if (includeGroup)

                return applicationDbContext.Set<Link>().Include(x => x.Group).AsNoTracking().FirstOrDefault(x => x.Id == linkId);

            else

                return applicationDbContext.Links.FirstOrDefault(x => x.Id == linkId);
        }
        public async Task<Link> GetLinkByIdAsync(int linkId, bool includeGroup = false)
        {
            return await Task.Run(() => GetLinkById(linkId, includeGroup));
        }

        public void AddLink(Link link)
        {
            if (link.Id == 0)
            {
                applicationDbContext.Add(link);
            }

            else
            {
                applicationDbContext.Entry(link).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
            }

            applicationDbContext.SaveChanges();
            link.ConvertedLink = "https://onlinelinkconverter.azurewebsites.net/Public/OLCRedirect?linkId=" + Convert.ToString(link.Id, 16);
            applicationDbContext.SaveChanges();
        }
        public async Task AddLinkAsync(Link link)
        {
            await Task.Run(() => AddLink(link));
        }

        public void ConvertLink(Link link)
        {
            //StringBuilder convertedLink = "https://olc.{0}.{1}/", Convert.ToString(link.GroupId, 16), Convert.ToString(link.Id, 16);
            //link.ConvertedLink = "https://olc."+ Convert.ToString(link.GroupId, 16) + "."+ Convert.ToString(link.Id, 16) + "/";
            link.ConvertedLink = "https://onlinelinkconverter.azurewebsites.net/Public/OLCRedirect?linkId=" + Convert.ToString(link.Id, 16);
        }
        public async Task ConvertLinkAsync(Link link)
        {
            await Task.Run(() => ConvertLink(link));
        }

        public void DeleteLinkById(int linkId)
        {
            applicationDbContext.Remove(applicationDbContext.Links.Find(linkId));
            applicationDbContext.SaveChanges();
        }
        public async Task DeleteLincAsync(int linkId)
        {
            await Task.Run(() => DeleteLinkById(linkId));
        }
    }
}
