﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using OnlineLinkConverter.Data.Interfaces;
using OnlineLinkConverter.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace OnlineLinkConverter.Data.Repository
{
    public class GroupRepository : IGroup
    {
        readonly ApplicationDbContext applicationDbContext;
        readonly IHttpContextAccessor httpContextAccessor;

        public GroupRepository(ApplicationDbContext applicationDbContext, IHttpContextAccessor httpContextAccessor)
        {
            this.applicationDbContext = applicationDbContext;
            this.httpContextAccessor = httpContextAccessor;
        }
        //public IEnumerable<Group> GetAllGroups => applicationDbContext.Groups;
        // public IEnumerable<Group> GetAllGroups => applicationDbContext.Groups.Where(x => String.Equals(x.UserId, httpContextAccessor.HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value));
        public IEnumerable<Group> GetAllGroups(string userId) => applicationDbContext.Groups.Where(x => x.UserId == userId);

        public Group GetGroupById(int groupId, bool includeLinks = false)
        {
            if (includeLinks)

                return applicationDbContext.Set<Group>().Include(x => x.Links).AsNoTracking().FirstOrDefault(x => x.Id == groupId);

            else

                return applicationDbContext.Groups.FirstOrDefault(x => x.Id == groupId);
        }
        public async Task<Group> GetGroupByIdAsync(int groupId, bool includeLinks = false)
        {
            return await Task.Run(() => GetGroupById(groupId, includeLinks));
        }

        public void AddGroup(Group group)
        {
            if (group.Id == 0)
            {
                applicationDbContext.Add(group);
            }

            else
            {
                applicationDbContext.Entry(group).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
            }

            applicationDbContext.SaveChanges();
        }
        public async Task AddGroupAsync(Group group)
        {
            await Task.Run(() => AddGroup(group));
        }

        public void DeleteGroupById(int groupId)
        {
            applicationDbContext.Remove(applicationDbContext.Groups.Find(groupId));
            applicationDbContext.SaveChanges();
        }
        public async Task DeleteGroupByIdAsync(int groupId)
        {
            await Task.Run(() => DeleteGroupById(groupId));
        }
    }
}
