﻿using OnlineLinkConverter.Data.Repository;
using OnlineLinkConverter.Data.ViewModels;
using OnlineLinkConverter.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using System.Security.Claims;
using System.Threading;
using System.Security.Principal;
using Microsoft.AspNetCore.Http;

namespace OnlineLinkConverter.Data.ViewServices
{
    public class GroupService
    {
        readonly RepositoryManager repositoryManager;
        readonly LinkService linkService;
        readonly IHttpContextAccessor httpContextAccessor;
        

        public GroupService(RepositoryManager repositoryManager, IHttpContextAccessor httpContextAccessor)
        {
            this.repositoryManager = repositoryManager;
            this.linkService = new LinkService(repositoryManager);
            this.httpContextAccessor = httpContextAccessor;
        }

        public List<GroupViewModel> GetGroupList(string userId)
        {
            var groups = repositoryManager.Groups.GetAllGroups(userId);
            List<GroupViewModel> groupsList = new List<GroupViewModel>();
            if (groups.Any())
            {
                foreach (var group in groups)
                {
                    groupsList.Add(GroupDBToViewModelById(group.Id));
                }
            }
            return groupsList;
        }
        public async Task<List<GroupViewModel>> GetGroupListAsync(string userId)
        {
            return await Task.Run(() => GetGroupList(userId));
        }

        public GroupViewModel GroupDBToViewModelById(int groupId)
        {
            var group = repositoryManager.Groups.GetGroupById(groupId, true);
            List<LinkViewModel> linksList = new List<LinkViewModel>();
            if (group.Links != null)
            {
                foreach (var link in group.Links)
                {
                    linksList.Add(linkService.LinkDBModelToView(link.Id));
                }
            }

            return new GroupViewModel() { Group = group, Links = linksList };
        }
        public async Task<GroupViewModel> GroupDBToViewModelByIdAsync(int groupId)
        {
            return await Task.Run(() => GroupDBToViewModelById(groupId));
        }

        public GroupEditModel GetGroupEditModel(int groupId)
        {
            if (groupId != 0)
            {
                var groupDB = repositoryManager.Groups.GetGroupById(groupId);
                var groupEditModel = new GroupEditModel()
                {
                    Id = groupDB.Id,
                    Name = groupDB.Name,
                    Description = groupDB.Description,
                    Links = groupDB.Links                   
                };

                return groupEditModel;
            }

            else
            {
                return new GroupEditModel() { };
            }
        }
        public async Task<GroupEditModel> GetGroupEditModelAsync(int groupId)
        {
            return await Task.Run(() => GetGroupEditModel(groupId));
        }

        public GroupViewModel SaveGroupEditModelToDB(GroupEditModel groupEditModel)
        {
            var userId = httpContextAccessor.HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value;            
            Group groupDbModel;
            if (groupEditModel.Id != 0)
            {
                groupDbModel = repositoryManager.Groups.GetGroupById(groupEditModel.Id);
            }

            else
            {
                groupDbModel = new Group();
            }

            groupDbModel.UserId = userId;
            groupDbModel.Description = groupEditModel.Description;
            groupDbModel.Name = groupEditModel.Name;
            groupDbModel.Links = groupEditModel.Links;

            repositoryManager.Groups.AddGroup(groupDbModel);

            return GroupDBToViewModelById(groupDbModel.Id);
        }
        public async Task<GroupViewModel> SaveGroupEditModelToDBAsync(GroupEditModel groupEditModel)
        {
            return await Task.Run(() => SaveGroupEditModelToDB(groupEditModel));
        }

        public GroupEditModel CreateNewGroupEditModel()
        {
            return new GroupEditModel() { };
        }
        public async Task<GroupEditModel> CreateNewGroupEditModelAsync()
        {
            return await Task.Run(() => CreateNewGroupEditModel());
        }

        public void RemoveGroupFromDb(int groupId)
        {
            repositoryManager.Groups.DeleteGroupById(groupId);
        }
        public async Task RemoveGroupFromDbAsync(int groupId)
        {
            await Task.Run(() => RemoveGroupFromDb(groupId));
        }
    }
}
