﻿using OnlineLinkConverter.Data.Repository;
using OnlineLinkConverter.Data.ViewModels;
using OnlineLinkConverter.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineLinkConverter.Data.ViewServices
{
    public class LinkService
    {
        readonly RepositoryManager repositoryManager;

        public LinkService(RepositoryManager repositoryManager)
        {
            this.repositoryManager = repositoryManager;
        }

        public LinkViewModel LinkDBModelToView(int linkId)
        {
            var link = new LinkViewModel()
            {
                Link = repositoryManager.Links.GetLinkById(linkId),
            };
            
            return link;
        }
        public async Task<LinkViewModel> LinkDBModelToViewAsync(int linkId)
        {
            return await Task.Run(() => LinkDBModelToView(linkId));
        }

        public LinkEditModel GetLinkEditModel(int linkId)
        {
            var dbModel = repositoryManager.Links.GetLinkById(linkId, true);
            var editModel = new LinkEditModel()
            {
                Id = dbModel.Id,
                GroupId = dbModel.GroupId,
                Description = dbModel.Description,
                ConvertedLink = dbModel.ConvertedLink,
                BaseLink = dbModel.BaseLink,
                IsActive = dbModel.IsActive

            };
            return editModel;
        }
        public async Task<LinkEditModel> GetLinkEditModelAsync(int linkId)
        {
            return await Task.Run(() => GetLinkEditModel(linkId));
        }

        public LinkViewModel SaveLinkEditModelToDb(LinkEditModel linkEditModel)
        {
            Link linkDbModel;
            if (linkEditModel.Id != 0)
            {
                linkDbModel = repositoryManager.Links.GetLinkById(linkEditModel.Id);
            }

            else
            {
                linkDbModel = new Link();
            }

            linkDbModel.Description = linkEditModel.Description;
            linkDbModel.BaseLink = linkEditModel.BaseLink;
            //linkDbModel.ConvertedLink = "https://localhost:44362/Groups/OLCRedirect?linkId=" + Convert.ToString(linkEditModel.Id, 16); ;
            linkDbModel.Group = linkEditModel.Group;
            linkDbModel.GroupId = linkEditModel.GroupId;
            linkDbModel.IsActive = linkEditModel.IsActive;

            repositoryManager.Links.AddLink(linkDbModel);

            return LinkDBModelToView(linkDbModel.Id);
        }
        public async Task<LinkViewModel> SaveLinkEditModelToDbAsync(LinkEditModel linkEditModel)
        {
            return await Task.Run(() => SaveLinkEditModelToDb(linkEditModel));
        }

        public LinkEditModel CreateNewLinkEditModel(int groupId)
        {
            return new LinkEditModel() { GroupId = groupId };
        }
        public async Task<LinkEditModel> CreateNewLinkEditModelAsync(int groupId)
        {
            return await Task.Run(() => CreateNewLinkEditModel(groupId));
        }

        public void RemoveLinkFromDb(int linkId)
        {
            repositoryManager.Links.DeleteLinkById(linkId);
        }
        public async Task RemoveLinkFromDbAsync(int linkId)
        {
            await Task.Run(() => RemoveLinkFromDb(linkId));
        }
    }
}
