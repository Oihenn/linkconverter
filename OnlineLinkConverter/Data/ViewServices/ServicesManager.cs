﻿using Microsoft.AspNetCore.Http;
using OnlineLinkConverter.Data.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace OnlineLinkConverter.Data.ViewServices
{
    public class ServicesManager
    {
        RepositoryManager repositoryManager;
        readonly GroupService groupService;
        readonly LinkService linkService;
        IHttpContextAccessor httpContextAccessor;

        public ServicesManager(RepositoryManager repositoryManager, IHttpContextAccessor httpContextAccessor)
        {
            this.repositoryManager = repositoryManager;
            groupService = new GroupService(repositoryManager, httpContextAccessor);
            linkService = new LinkService(repositoryManager);

            this.httpContextAccessor = httpContextAccessor;
        }

        public GroupService Groups { get { return groupService; } }
        public LinkService Links { get { return linkService; } }
        //public bool IsAuthorized()
        //{
        //    if (httpContextAccessor.HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value != null)
        //    {
        //        return true;
        //    }
        //    return false;
        //}
    }
}
