﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using OnlineLinkConverter.Data.Interfaces;
using OnlineLinkConverter.Data.Repository;
using OnlineLinkConverter.Data.ViewModels;
using OnlineLinkConverter.Data.ViewServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using static OnlineLinkConverter.Data.Enums.PageEnums;

namespace OnlineLinkConverter.Controllers
{
    [Authorize]
    public class GroupsController : Controller
    {
        private readonly RepositoryManager repositoryManager;
        private readonly ServicesManager serviceManager;
        private readonly UserManager<IdentityUser> userManager;


        public GroupsController(RepositoryManager repositoryManager, IHttpContextAccessor httpContextAccessor, UserManager<IdentityUser> userManager)
        {
            this.userManager = userManager;
            this.repositoryManager = repositoryManager;
            serviceManager = new ServicesManager(repositoryManager, httpContextAccessor);
        }

        public async Task<IActionResult> Details(int pageId, PageType pageType)
        {
            PageViewModel pageViewModel;
            switch (pageType)
            {
                case PageType.Group: pageViewModel = await serviceManager.Groups.GroupDBToViewModelByIdAsync(pageId); break;
                case PageType.Link: pageViewModel = await serviceManager.Links.LinkDBModelToViewAsync(pageId); break;
                default: pageViewModel = null; break;
            }
            ViewBag.PageType = pageType;
            return View(pageViewModel);
        }

        [HttpGet]
        public async Task<IActionResult> PageEditor(int pageId, PageType pageType, int groupId = 0)
        {            

            PageEditModel pageEditModel;
            switch (pageType)
            {
                case PageType.Group: 
                    if(pageId != 0)
                        pageEditModel = await serviceManager.Groups.GetGroupEditModelAsync(pageId); 
                    else
                        pageEditModel = await serviceManager.Groups.CreateNewGroupEditModelAsync();
                    break;
                case PageType.Link:
                    if (pageId != 0)
                        pageEditModel = await serviceManager.Links.GetLinkEditModelAsync(pageId);
                    else
                        pageEditModel = await serviceManager.Links.CreateNewLinkEditModelAsync(groupId);
                    break;
                default: pageEditModel = null; break;
            }
            ViewBag.PageType = pageType;
            return View(pageEditModel);
        }

        [HttpPost]
        public async Task<IActionResult> AddGroup(GroupEditModel model)
        {
            await serviceManager.Groups.SaveGroupEditModelToDBAsync(model);
            return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        public async Task<IActionResult> AddLink(LinkEditModel model)
        {
            await serviceManager.Links.SaveLinkEditModelToDbAsync(model);
            return RedirectToAction("Index", "Home");
        }

        public async Task<IActionResult> RemoveGroup(int groupId)
        {
            await serviceManager.Groups.RemoveGroupFromDbAsync(groupId);
            return RedirectToAction("Index", "Home");
        }

        public async Task<IActionResult> RemoveLink(int linkId)
        {
            await serviceManager.Links.RemoveLinkFromDbAsync(linkId);
            return RedirectToAction("Index", "Home");
        }

        public async Task<IActionResult> AllLinks()
        {
            var user = await userManager.GetUserAsync(User);
            List<GroupViewModel> groups = await serviceManager.Groups.GetGroupListAsync(user.Id);
            return View(groups);
        }

        public async Task<IActionResult> OLCRedirect(string groupId, string linkId)
        {
            int intLinkId = Convert.ToInt32(linkId, 16);
            LinkViewModel model = await serviceManager.Links.LinkDBModelToViewAsync(intLinkId);
            //string page = "https://localhost:44362/";
            string page = "https://onlinelinkconverter.azurewebsites.net/";
            if (model.Link.IsActive)
            {
                page = model.Link.BaseLink;
            }
            return Redirect(page);
        }
    }
}