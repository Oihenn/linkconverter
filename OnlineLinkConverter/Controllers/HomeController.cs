﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using OnlineLinkConverter.Data.Repository;
using OnlineLinkConverter.Data.ViewModels;
using OnlineLinkConverter.Data.ViewServices;
using OnlineLinkConverter.Models;

namespace OnlineLinkConverter.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private readonly RepositoryManager repositoryManager;
        private readonly ServicesManager serviceManager;
        private readonly UserManager<IdentityUser> userManager;

        public HomeController(RepositoryManager repositoryManager, IHttpContextAccessor httpContextAccessor, UserManager<IdentityUser> userManager)
        {
            this.userManager = userManager;
            this.repositoryManager = repositoryManager;
            serviceManager = new ServicesManager(repositoryManager, httpContextAccessor);
        }

        public async Task<IActionResult> Index()
        {
            var user = await userManager.GetUserAsync(User);
            List<GroupViewModel> groups = await serviceManager.Groups.GetGroupListAsync(user.Id);
            return View(groups);
        }
                    

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
