﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using OnlineLinkConverter.Data.Interfaces;
using OnlineLinkConverter.Data.Repository;
using OnlineLinkConverter.Data.ViewModels;
using OnlineLinkConverter.Data.ViewServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using static OnlineLinkConverter.Data.Enums.PageEnums;

namespace OnlineLinkConverter.Controllers
{
    public class PublicController : Controller
    {
        private readonly ServicesManager serviceManager;

        public PublicController(RepositoryManager repositoryManager, IHttpContextAccessor httpContextAccessor)
        {
            serviceManager = new ServicesManager(repositoryManager, httpContextAccessor);
        }
        public async Task<IActionResult> OLCRedirect(string groupId, string linkId)
        {
            int intLinkId = Convert.ToInt32(linkId, 16);
            LinkViewModel model = await serviceManager.Links.LinkDBModelToViewAsync(intLinkId);
            //string page = "https://localhost:44362/";
            string page = "https://onlinelinkconverter.azurewebsites.net/";
            if (model.Link.IsActive)
            {
                page = model.Link.BaseLink;
            }
            return Redirect(page);
        }
    }
}
